var _ = require('lodash');

// SMA OK -,-
exports.index = function (data) {
  var cloneData = _.clone(data.requests);
  var monthAvg = 4;
  var result = {
    metode: 'DOUBLE MOVING AVERAGE',
    nextForecast: 0,
    MSE: 0,
    holder: []
  };
  var st, sst, a, b, fi, ei, ein, ei2 = null;
  var counter = monthAvg;
  var secondCounter = 0;
  var counter2nd = 0;
  var listEAK = [];
  var listPA = [];
  var listFC = [];
  for (var i = 0; i < cloneData.length; i++) {
    if (i < 3) {
      listPA.push(cloneData[i]);
      result.holder.push({
        yt: cloneData[i],
        st: null,
        sst: null,
        a: null,
        b: null,
        fi: null,
        ei: null,
        ein: null,
        ei2: null
      });
    } else {
      var lastAB = { a: result.holder[i - 1].a, b: result.holder[i - 1].b };
      listPA.push(cloneData[i]);
      var processed = processToNext(i, cloneData, cloneData[i], counter2nd, secondCounter, monthAvg, listFC, listPA, lastAB);
      if (processed.ei2) listEAK.push(processed.ei2);
      listFC = processed.listFC;
      // listFC.push(processed.st);
      result.holder.push({
        yt: processed.yt,
        st: processed.st,
        sst: processed.sst,
        a: processed.a,
        b: processed.b,
        fi: processed.fi,
        ei: processed.ei,
        ein: processed.ein,
        ei2: processed.ei2
      });
      counter2nd++;
      if (i > 5) secondCounter++;
    }
  }
  var tempNFC = result.holder[11].a + result.holder[11].b;
  result.nextForecast = Math.ceil(tempNFC);
  result.MSE = _.sum(listEAK) / (monthAvg + 1);

  return result;
};

function processToNext(pos, data, yt, from, to, avg, fc, pa, lastAB) {
  var st, sst, a, b, fi, ei, ein, ei2 = null;
  var st = ekstrakST(pa, avg, from);
  fc.push(st);
  if (pos > 5) {
    console.log(pos, 'listFC', fc);
    sst = SSTEkstrak(fc, avg, to);
    a = 2 * st - sst;
    b = (2 / 3) * (st - sst);
    if (pos > 6) {
      fi = lastAB.a + lastAB.b;
      ei = fi - yt;
      ein = Math.abs(ei);
      ei2 = Math.pow(ein, 2);
    } else {
      fi = null;
      ei = null;
      ein = null;
      ei2 = null;
    }
  } else {
    sst = null;
    a = null;
    b = null;
    fi = null;
    ei = null;
    ein = null;
    ei2 = null;
  }

  return { yt: yt, st: st, sst: sst, a: a, b: b, fi: fi, ei: ei, ein: ein, ei2: ei2, listFC: fc };
}

function ekstrakST(pa, avg, counter) {
  var lastIndex = pa.length;
  var value = 0;
  for (var i = counter; i < lastIndex; i++) {
    value += pa[i];
  }
  return value / avg;
}

function SSTEkstrak(fc, avg, counter) {
  var lastIndex = fc.length;
  var nilai = 0;
  for (var i = counter; i < lastIndex; i++) {
    nilai += fc[i];
  }
  return nilai / avg;
}

// listPA.push(cloneData[i]);
//       if (listPA.length === monthAvg) {
//         // st = _.sum(listPA) / monthAvg;
//         st = ekstrakST(listPA, monthAvg, true, counter2nd);
//         listFC.push(st);
//       }

//       if (listFC.length === monthAvg) {
//         sst = _.sum(listFC) / monthAvg;
//         a = 2 * st - sst;
//         b = (2 / 3) * (st - sst);
//       }
//       result.holder.push({
//         yt: cloneData[i],
//         st: st,
//         sst: sst,
//         a: a,
//         b: b,
//         fi: null,
//         ei: null,
//         ein: null,
//         ei2: null
//       });
//       counter2nd++;