var _ = require('lodash');

exports.index = function (data) {
  var cloneData = _.clone(data.requests);
  var n = 12;
  var result = {
    metode: 'REGRESI LINEAR',
    nextForecast: 0,
    MSE: 0,
    holder: []
  };
  var temp = [];
  var tyt, t2 = null;

  for (var i = 0; i < cloneData.length; i++) {
    tyt = cloneData[i] * (i + 1);
    t2 = Math.pow(i + 1, 2);
    temp.push({
      periode: i + 1,
      yt: cloneData[i],
      tyt: tyt,
      t2: t2,
      fi: 0,
      ei: 0,
      ein: 0,
      ei2: 0
    });
  }

  var processed = processToResult(temp, n);
  result.holder = processed.data;
  result.MSE = processed.MSE;
  result.nextForecast = processed.forecast;
  return result;
};

function processToResult(data, n) {
  var total = {
    periode: 0,
    yt: 0,
    tyt: 0,
    t2: 0
  }
  var fi, ei, ein, ei2 = 0;
  var listEAK = [];
  for (var i = 0; i < data.length; i++) {
    total.periode += data[i].periode;
    total.yt += data[i].yt;
    total.tyt += data[i].tyt;
    total.t2 += data[i].t2;
  }
  var b = (n * total.tyt - (total.periode * total.yt)) / ((n * total.t2) - (Math.pow(total.periode, 2)));
  // var b = (12 * 4030 - (78 * 584)) / ((12 * 650) - (78 * 78));
  var a = (total.yt / n) - (b * total.periode / n);

  for (var j = 0; j < data.length; j++) {
    fi = a + (b * (j + 1));
    ei = data[j].yt - fi;
    ein = Math.abs(ei);
    ei2 = Math.pow(ein, 2);

    listEAK.push(ei2);
    data[j].fi = parseFloat(fi).toFixed(6);
    data[j].ei = parseFloat(ei).toFixed(6);
    data[j].ein = parseFloat(ein).toFixed(6);
    data[j].ei2 = parseFloat(ei2).toFixed(6);
  }
  var MSE = _.sum(listEAK) / n;
  var tempNFC = (a + (b * (n + 1)));
  var forecast = Math.ceil(tempNFC);
  return { data: data, MSE: MSE, forecast: forecast };
}