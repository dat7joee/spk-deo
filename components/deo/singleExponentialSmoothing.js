var _ = require('lodash');

// SES OK!
exports.index = function (data) {
  var cloneData = _.clone(data.requests);
  var alpha = 0.5;
  var result = {
    metode: 'SINGLE EXPONENTIAL SMOOTHING',
    nextForecast: 0,
    MSE: 0,
    holder: []
  };
  var st, ei, ein, ei2 = null;
  var listEAK = [];

  for (var i = 0; i < cloneData.length; i++) {
    if (i === 0) {
      result.holder.push({
        yt: cloneData[i],
        st: cloneData[i],
        ei: null,
        ein: null,
        ei2: null
      });
    } else {
      st = (result.holder[i - 1].yt * alpha) + (result.holder[i - 1].st * (1 - alpha));
      ei = cloneData[i] - st;
      ein = Math.abs(ei);
      ei2 = Math.pow(ein, 2);
      listEAK.push(ei2);
      result.holder.push({
        yt: cloneData[i],
        st: st,
        ei: ei,
        ein: ein,
        ei2: ei2
      });
    }
  }
  var tempNFC = (result.holder[11].yt * alpha) + (result.holder[11].st * (1 - alpha));
  result.nextForecast = Math.ceil(tempNFC);
  result.MSE = _.sum(listEAK) / 11;

  return result;
};