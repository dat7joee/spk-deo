var _ = require('lodash');

// SMA OK -,-
exports.index = function (data) {
  var cloneData = _.clone(data.requests);
  var monthAvg = 6;
  var result = {
    metode: 'SINGLE MOVING AVERAGE',
    nextForecast: 0,
    MSE: 0,
    holder: []
  };
  var st, ei, ein, ei2 = null;
  var counter = 0;
  var limitCounter = monthAvg;
  var listEAK = [];
  var listPA = [];
  for (var i = 0; i < cloneData.length; i++) {
    if (i < 6) {
      result.holder.push({
        yt: cloneData[i],
        st: null,
        ei: null,
        ein: null,
        ei2: null
      });
    } else {
      var processed = processToNext(cloneData, cloneData[i], counter, limitCounter, monthAvg);
      listEAK.push(processed.ei2);
      listPA.push(processed.yt);
      result.holder.push({
        yt: processed.yt,
        st: processed.st,
        ei: processed.ei,
        ein: processed.ein,
        ei2: processed.ei2
      });
      counter++;
      limitCounter++;
    }
  }
  var tempNFC = _.sum(listPA) / monthAvg;
  result.nextForecast = Math.ceil(tempNFC);
  result.MSE = _.sum(listEAK) / monthAvg;

  return result;
};

function processToNext(data, yt, from, to, avg) {
  var value = 0;
  for (var i = from; i < to; i++) {
    value += data[i];
  }
  var st = value / avg;
  var ei = yt - st;
  var ein = Math.abs(ei);
  var ei2 = Math.pow(ein, 2);

  return { yt: yt, st: st, ei: ei, ein: ein, ei2: ei2 };
}