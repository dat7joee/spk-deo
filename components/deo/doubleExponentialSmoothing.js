var _ = require('lodash');

exports.index = function (data) {
  var cloneData = _.clone(data.requests);
  var alpha = 0.5;
  var result = {
    metode: 'DOUBLE EXPONENTIAL SMOOTHING',
    nextForecast: 0,
    MSE: 0,
    holder: []
  };
  var st, sst, a, b, fi, ei, ein, ei2 = null;
  var listEAK = [];

  for (var i = 0; i < cloneData.length; i++) {
    if (i === 0) {
      result.holder.push({
        yt: cloneData[i],
        st: cloneData[i],
        sst: cloneData[i],
        a: cloneData[i],
        b: 0,
        fi: null,
        ei: null,
        ein: null,
        ei2: null
      });
    } else {
      st = alpha * cloneData[i] + alpha * result.holder[i - 1].st;
      sst = alpha * st + alpha * result.holder[i - 1].sst;
      a = 2 * st - sst;
      b = (alpha / alpha) * (st - sst);
      fi = result.holder[i - 1].a + result.holder[i - 1].b;
      ei = cloneData[i] - fi;
      ein = Math.abs(ei);
      ei2 = Math.pow(ein, 2);

      if (i > 5 && i < 11) listEAK.push(ei2);
      result.holder.push({
        yt: cloneData[i],
        st: st,
        sst: sst,
        a: a,
        b: b,
        fi: fi,
        ei: ei,
        ein: ein,
        ei2: ei2
      });
    }
  }
  var tempNFC = result.holder[11].a + result.holder[11].b;
  result.nextForecast = Math.ceil(tempNFC);
  result.MSE = parseFloat(_.sum(listEAK) / 5).toFixed(6);

  return result;
};