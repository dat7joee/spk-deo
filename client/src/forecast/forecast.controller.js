angular.module('spk_deo')
  .controller('ForecastCtrl', function ($scope, $http, NgTableParams, $state) {
    $scope.model = {};
    $scope.holder = {};
    $scope.model.requests = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    // $scope.model.requests = [55, 65, 70, 62, 59, 54, 60, 68, 72, 70, 67, 65]; //test untuk Pola Siklis
    // $scope.model.requests = [25, 35, 33, 30, 39, 41, 45, 48, 46, 52, 50, 55]; //test untuk DES & DMA / Pola Trend
    // $scope.model.requests = [35, 29, 48, 60, 55, 44, 42, 35, 75, 56, 55, 50] test untuk Regresi Linear / Pola Musiman
    // $scope.model.requests = [49, 65, 60, 53, 66, 46, 40, 50, 57, 42, 46, 38]; //test untuk SMA dan SES / Pola Horizontal
    $scope.smaHeaders = [''];
    $scope.sesHeaders = ['y(t)', "s'(t)", 'ei', '|ei|', '|ei|^2'];
    $scope.rlHeaders = ['t', 'y(t)', 'ty(t)', '𝑡^2', 'Fi', 'ei', '|ei|', '|ei|^2'];
    $scope.desHeaders = ['y(t)', "s'(t)", "s''(t)", 'a', 'b', 'Fi', 'ei', '|ei|', '|ei|^2'];
    $scope.dmaHeaders = [];

    $scope.myHeaders = [];
    $scope.myYears = [];
    $scope.processing = false;
    $scope.eoqProcessing = false;

    $scope.getProducts = function (value) {
      return $http.get('/api/products/search?name=' + value).then(function (response) {
        return response.data;
      }).catch(function (err) {
        console.log(err);
      });
    };

    $scope.number = 5;
    $scope.getNumber = function (num) {
      return new Array(num);
    };
    $scope.forecast = {};

    $scope.onProcessForecast = function () {
      $scope.processing = true;
      $http.post('/api/test', $scope.model).then(function (response) {
        console.log('response', response);
        if (response.data.metode === 'REGRESI LINEAR') $scope.myHeaders = $scope.rlHeaders;
        if (response.data.metode === 'SINGLE EXPONENTIAL SMOOTHING' || response.data.metode === 'SINGLE MOVING AVERAGE') $scope.myHeaders = $scope.sesHeaders;
        if (response.data.metode === 'DOUBLE EXPONENTIAL SMOOTHING' || response.data.metode === 'DOUBLE MOVING AVERAGE') $scope.myHeaders = $scope.desHeaders;
        $scope.forecast = response.data;
        $scope.processing = false;
      }).catch(function (err) {
        console.log(err);
        alert(err.data.message);
      });
    };

    $scope.getYears = function () {
      $http.get('/api/test/years').then(function (response) {
        $scope.myYears = response.data;
      }).catch(function (err) {
        if (err) console.log(err);
      });
    };

    $scope.onSaveForecast = function () {
      if (confirm('Apakah Kamu Yakin ingin Menyimpan Data Peramalan Ini untuk Product ' + $scope.model.product.name + ' ?')) {
        $scope.forecast.month = $scope.model.month;
        $scope.forecast.year = $scope.model.year;
        $http.post('/api/forecast', $scope.forecast).then(function (response) {
          alert('Berhasil Menyimpan Peramalan!');
        }).catch(function (err) {
          if (err) alert('Ooppss.. Ada Error Saat Menghubungkan Ke Server!');
          console.log('err', err);
        });
      }
    };

    $scope.table = new NgTableParams({
      count: 10,
      page: 1
    }, { getData: getData });

    function getData(params) {
      var name = params.filter().name ? params.filter().name : '';
      return $http.get('/api/forecast?page=' + params.page() + '&limit=' + params.count()).then(function (response) {
        params.total(response.headers('x-pagination-total-count'));
        return response.data;
      }).catch(function (err) {
        if (err) console.log(err);
      });
    }

    $scope.onDelete = function (id, material) {
      if (confirm('Apakah Kamu Yakin Ingin Menghapus Data Ini ? ' + material.name)) {
        $http.delete('/api/forecast/' + id).then(function (response) {
          alert('Berhasil Menghapus Data!');
          $state.reload();
        }).catch(function (err) {
          console.log(err);
          alert('Oopss!! Ada Kesalahan Saat Menghapus Data Dari Server!');
        });
      }
    };

    $scope.onProcessEOQ = function () {
      if (!$scope.holder.month && !$scope.holder.year) {
        alert('Input Bulan dan Tahun untuk Melakukan Proses EOQ');
        return;
      }
      $http.get('/api/forecast/eoq?month=' + $scope.holder.month + '&year=' + $scope.holder.year).then(function (response) {
        $scope.eoq = response.data.plates;
        $scope.eoqProcessing = true;
        $scope.yearForecast = response.data.year;
        $scope.monthForecast = response.data.month;
        console.log('dt', response.data);
      }).catch(function (err) {
        console.log(err);
        alert('Data Peramalan Kosong!');
      });
    };

    $scope.onResetForecast = function () {
      if (confirm('WARNING!!! APAKAH KAMU YAKIN INGIN MERESET DATA PERAMALAN ?')) {
        $http.get('/api/forecast/reset').then(function (response) {
          console.log('response', response);
        }).catch(function (err) {
          console.log('err', err);
          alert('Oopss...!! Terjadi Error Ketika Menghubungi Server!');
        });
      }
    };

    $scope.checkMonth = function (month) {
      month = Number(month);
      switch (month) {
        case 1:
          return 'Februari';
          break;
        case 2:
          return 'Maret';
          break;
        case 3:
          return 'April';
          break;
        case 4:
          return 'Mei';
          break;
        case 5:
          return 'Juni';
          break;
        case 6:
          return 'Juli';
          break;
        case 7:
          return 'Agustus';
          break;
        case 8:
          return 'September';
          break;
        case 9:
          return 'Oktober';
          break;
        case 10:
          return 'November';
          break;
        case 11:
          return 'Desember';
          break;
        default:
          return 'Januari';
      }
    }

    $scope.months = [
      {
        title: 'Januari',
        value: '0'
      },
      {
        title: 'Februari',
        value: '1'
      },
      {
        title: 'Maret',
        value: '2'
      },
      {
        title: 'April',
        value: '3'
      },
      {
        title: 'Mei',
        value: '4'
      },
      {
        title: 'Juni',
        value: '5'
      },
      {
        title: 'Juli',
        value: '6'
      },
      {
        title: 'Agustus',
        value: '7'
      },
      {
        title: 'September',
        value: '8'
      },
      {
        title: 'Oktober',
        value: '9'
      },
      {
        title: 'November',
        value: '10'
      },
      {
        title: 'Desember',
        value: '11'
      }
    ];
  });