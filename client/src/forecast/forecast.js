angular.module('spk_deo')
  .config(function ($stateProvider) {
    $stateProvider
      .state('forecast', {
        abstract: true,
        templateUrl: 'src/templates/_layout.html'
      })
      .state('forecast.index', {
        url: '/forecasts',
        templateUrl: 'src/forecast/forecast.html',
        controller: 'ForecastCtrl'
      })
      .state('forecast.data', {
        url: '/forecasts/data',
        templateUrl: 'src/forecast/forecast-data.html',
        controller: 'ForecastCtrl'
      });
  });