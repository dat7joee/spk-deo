angular.module('spk_deo', ['ui.bootstrap', 'ui.router', 'ngTable'])
  .config(function ($stateProvider, $urlRouterProvider, $httpProvider) {
    $urlRouterProvider.otherwise('/');

    $stateProvider
      .state('index', {
        url: '/',
        templateUrl: 'src/templates/_main.html'
      })
  })
  .controller('MasterCtrl', function ($scope) {
    var isLoggedIn = localStorage.isLoggedIn;
    if (!isLoggedIn) {
      window.open("login.html", "_self");
    }

    $scope.onLogout = function () {
      localStorage.setItem('isLoggedIn', false);
      window.open("login.html", "_self");
    }
  });