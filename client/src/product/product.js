angular.module('spk_deo')
  .config(function ($stateProvider) {
    $stateProvider
      .state('product', {
        abstract: true,
        templateUrl: 'src/templates/_layout.html'
      })
      .state('product.index', {
        url: '/products',
        templateUrl: 'src/product/product.html',
        controller: 'ProductCtrl'
      })
      .state('product.new', {
        url: '/products/new',
        templateUrl: 'src/product/form/product.html',
        controller: 'ProductFormCtrl'
      })
      .state('product.edit', {
        url: '/products/edit/:id',
        templateUrl: 'src/product/form/product.html',
        controller: 'ProductFormCtrl'
      });
  });