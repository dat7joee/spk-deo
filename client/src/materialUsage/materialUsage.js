angular.module('spk_deo')
  .config(function ($stateProvider) {
    $stateProvider
      .state('materialUsage', {
        abstract: true,
        templateUrl: 'src/templates/_layout.html'
      })
      .state('materialUsage.index', {
        url: '/materialUsages',
        templateUrl: 'src/materialUsage/materialUsage.html',
        controller: 'MaterialUsageCtrl'
      })
      .state('materialUsage.new', {
        url: '/materialUsages/new',
        templateUrl: 'src/materialUsage/form/materialUsage.html',
        controller: 'MaterialUsageFormCtrl'
      })
      .state('materialUsage.edit', {
        url: '/materialUsages/edit/:id',
        templateUrl: 'src/materialUsage/form/materialUsage.html',
        controller: 'MaterialUsageFormCtrl'
      });
  });