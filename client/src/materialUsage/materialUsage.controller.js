angular.module('spk_deo')
  .controller('MaterialUsageCtrl', function ($scope, $http, NgTableParams, $state) {

    $scope.table = new NgTableParams({
      count: 10,
      page: 1
    }, { getData: getData });

    function getData(params) {
      var productName = params.filter().productName ? params.filter().productName : '';
      return $http.get('/api/materialUsages?page=' + params.page() + '&limit=' + params.count() + '&productName=' + productName).then(function (response) {
        params.total(response.headers('x-pagination-total-count'));
        return response.data;
      }).catch(function (err) {
        if (err) console.log(err);
      });
    }

    $scope.onDelete = function (id, materialUsage) {
      if (confirm('Apakah Kamu Yakin Ingin Menghapus Data Ini ? ' + materialUsage.name)) {
        $http.delete('/api/materialUsages/' + id).then(function (response) {
          alert('Berhasil Menghapus Data!');
          $state.reload();
        }).catch(function (err) {
          console.log(err);
          alert('Oopss!! Ada Kesalahan Saat Menghapus Data Dari Server!');
        });
      }
    };

  });