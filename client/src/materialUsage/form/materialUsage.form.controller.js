angular.module('spk_deo')
  .controller('MaterialUsageFormCtrl', function ($scope, $http, $state, $stateParams) {
    $scope.model = {};
    $scope.model.parts = [];
    $scope.holder = {};
    $scope.loadingLocations = false;
    $scope.noResults = false;

    if ($stateParams.id) {
      $scope.onEdit = true;
      $http.get('/api/materialUsages/' + $stateParams.id).then(function (response) {
        $scope.model = response.data;
      }).catch(function (err) {
        $state.go('^.index');
        console.log('err', err);
        alert('Oopss!! Data Tidak Ditemukan!');
      });
    }

    $scope.onSave = function () {
      if (confirm('Apakah Kamu Yakin Ingin Menyimpan Data Ini ?')) {
        if (!$stateParams.id) {
          $http.post('/api/materialUsages', $scope.model).then(function (response) {
            alert('Berhasil Menyimpan!');
            $state.go('^.index');
          }).catch(function (err) {
            console.log(err);
            alert('Oopss! Error Terjadi Ketika Menghubungi Server!');
          });
        } else {
          $http.put('/api/materialUsages/' + $stateParams.id, $scope.model).then(function (response) {
            alert('Berhasil Menyimpan!');
            $state.go('^.index');
          }).catch(function (err) {
            console.log(err);
            alert('Oopss! Error Terjadi Ketika Menghubungi Server!');
          });
        }
      }
    };

    $scope.onAddPart = function () {
      if (!$scope.holder.part && !$scope.holder.usage) return;

      $scope.model.parts.push({
        material: $scope.holder.part,
        usage: $scope.holder.usage
      });
      $scope.holder = {};
    };

    $scope.onRemovePart = function (index) {
      $scope.model.parts.splice(index, 1);
    };

    $scope.getProducts = function (value) {
      return $http.get('/api/products/search?name=' + value).then(function (response) {
        return response.data;
      }).catch(function (err) {
        console.log(err);
      });
    };

    $scope.getMaterials = function (value) {
      return $http.get('/api/materials/search?name=' + value).then(function (response) {
        return response.data;
      }).catch(function (err) {
        console.log(err);
      });
    };
  });