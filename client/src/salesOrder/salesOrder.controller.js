angular.module('spk_deo')
  .controller('SalesOrderCtrl', function ($scope, $http, NgTableParams, $state) {

    $scope.table = new NgTableParams({
      count: 10,
      page: 1
    }, { getData: getData });

    function getData(params) {
      var month = params.filter().month ? params.filter().month : '';
      var year = params.filter().year ? params.filter().year : '';
      return $http.get('/api/salesOrders?page=' + params.page() + '&limit=' + params.count() + '&month=' + month + '&year=' + year).then(function (response) {
        params.total(response.headers('x-pagination-total-count'));
        return response.data;
      }).catch(function (err) {
        if (err) console.log(err);
      });
    }

    $scope.checkMonth = function (month) {
      month = Number(month);
      switch (month) {
        case 1:
          return 'Februari';
          break;
        case 2:
          return 'Maret';
          break;
        case 3:
          return 'April';
          break;
        case 4:
          return 'Mei';
          break;
        case 5:
          return 'Juni';
          break;
        case 6:
          return 'Juli';
          break;
        case 7:
          return 'Agustus';
          break;
        case 8:
          return 'September';
          break;
        case 9:
          return 'Oktober';
          break;
        case 10:
          return 'November';
          break;
        case 11:
          return 'Desember';
          break;
        default:
          return 'Januari';
      }
    }

    $scope.onDelete = function (id, salesOrder) {
      if (confirm('Apakah Kamu Yakin Ingin Menghapus Data Ini ? ' + salesOrder.name)) {
        $http.delete('/api/salesOrders/' + id).then(function (response) {
          alert('Berhasil Menghapus Data!');
          $state.reload();
        }).catch(function (err) {
          console.log(err);
          alert('Oopss!! Ada Kesalahan Saat Menghapus Data Dari Server!');
        });
      }
    };

  });