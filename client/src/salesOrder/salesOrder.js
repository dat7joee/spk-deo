angular.module('spk_deo')
  .config(function ($stateProvider) {
    $stateProvider
      .state('salesOrder', {
        abstract: true,
        templateUrl: 'src/templates/_layout.html'
      })
      .state('salesOrder.index', {
        url: '/salesOrders',
        templateUrl: 'src/salesOrder/salesOrder.html',
        controller: 'SalesOrderCtrl'
      })
      .state('salesOrder.new', {
        url: '/salesOrders/new',
        templateUrl: 'src/salesOrder/form/salesOrder.html',
        controller: 'SalesOrderFormCtrl'
      })
      .state('salesOrder.edit', {
        url: '/salesOrders/edit/:id',
        templateUrl: 'src/salesOrder/form/salesOrder.html',
        controller: 'SalesOrderFormCtrl'
      });
  });