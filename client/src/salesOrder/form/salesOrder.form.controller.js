angular.module('spk_deo')
  .controller('SalesOrderFormCtrl', function ($scope, $http, $state, $stateParams) {
    $scope.model = {};
    $scope.model.products = [];
    if ($stateParams.id) {
      $scope.onEdit = true;
      $http.get('/api/salesOrders/' + $stateParams.id).then(function (response) {
        $scope.model = response.data;
      }).catch(function (err) {
        $state.go('^.index');
        console.log('err', err);
        alert('Oopss!! Data Tidak Ditemukan!');
      });
    }

    $scope.onSave = function () {
      if (confirm('Apakah Kamu Yakin Ingin Menyimpan Data Ini ?')) {
        if (!$stateParams.id) {
          $http.post('/api/salesOrders', $scope.model).then(function (response) {
            alert('Berhasil Menyimpan!');
            $state.go('^.index');
          }).catch(function (err) {
            console.log(err);
            alert('Oopss! Error Terjadi Ketika Menghubungi Server!');
          });
        } else {
          $http.put('/api/salesOrders/' + $stateParams.id, $scope.model).then(function (response) {
            alert('Berhasil Menyimpan!');
            $state.go('^.index');
          }).catch(function (err) {
            console.log(err);
            alert('Oopss! Error Terjadi Ketika Menghubungi Server!');
          });
        }
      }
    };

    $scope.onAddProduct = function () {
      if (!$scope.holder.product && !$scope.holder.sale) return;

      $scope.model.products.push({
        product: $scope.holder.product,
        sale: $scope.holder.sale
      });
      $scope.holder = {};
    };

    $scope.onRemoveProduct = function (index) {
      $scope.model.products.splice(index, 1);
    };

    $scope.getProducts = function (value) {
      return $http.get('/api/products/search?name=' + value).then(function (response) {
        return response.data;
      }).catch(function (err) {
        console.log(err);
      });
    };

    $scope.months = [
      {
        title: 'Januari',
        value: '0'
      },
      {
        title: 'Februari',
        value: '1'
      },
      {
        title: 'Maret',
        value: '2'
      },
      {
        title: 'April',
        value: '3'
      },
      {
        title: 'Mei',
        value: '4'
      },
      {
        title: 'Juni',
        value: '5'
      },
      {
        title: 'Juli',
        value: '6'
      },
      {
        title: 'Agustus',
        value: '7'
      },
      {
        title: 'September',
        value: '8'
      },
      {
        title: 'Oktober',
        value: '9'
      },
      {
        title: 'November',
        value: '10'
      },
      {
        title: 'Desember',
        value: '11'
      },
    ];
  });