angular.module('spk_deo')
  .config(function ($stateProvider) {
    $stateProvider
      .state('supplier', {
        abstract: true,
        templateUrl: 'src/templates/_layout.html'
      })
      .state('supplier.index', {
        url: '/suppliers',
        templateUrl: 'src/supplier/supplier.html',
        controller: 'SupplierCtrl'
      })
      .state('supplier.new', {
        url: '/suppliers/new',
        templateUrl: 'src/supplier/form/supplier.html',
        controller: 'SupplierFormCtrl'
      })
      .state('supplier.edit', {
        url: '/suppliers/edit/:id',
        templateUrl: 'src/supplier/form/supplier.html',
        controller: 'SupplierFormCtrl'
      });
  });