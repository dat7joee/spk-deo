angular.module('spk_deo')
  .controller('SupplierFormCtrl', function ($scope, $http, $state, $stateParams) {
    $scope.model = {};
    if ($stateParams.id) {
      $scope.onEdit = true;
      $http.get('/api/suppliers/' + $stateParams.id).then(function (response) {
        $scope.model = response.data;
      }).catch(function (err) {
        $state.go('^.index');
        console.log('err', err);
        alert('Oopss!! Data Tidak Ditemukan!');
      });
    }

    $scope.onSave = function () {
      if (confirm('Apakah Kamu Yakin Ingin Menyimpan Data Ini ?')) {
        if (!$stateParams.id) {
          $http.post('/api/suppliers', $scope.model).then(function (response) {
            alert('Berhasil Menyimpan!');
            $state.go('^.index');
          }).catch(function (err) {
            console.log(err);
            alert('Oopss! Error Terjadi Ketika Menghubungi Server!');
          });
        } else {
          $http.put('/api/suppliers/' + $stateParams.id, $scope.model).then(function (response) {
            alert('Berhasil Menyimpan!');
            $state.go('^.index');
          }).catch(function (err) {
            console.log(err);
            alert('Oopss! Error Terjadi Ketika Menghubungi Server!');
          });
        }
      }
    };
  });