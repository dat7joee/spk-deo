angular.module('spk_deo')
  .controller('SupplierCtrl', function ($scope, $http, NgTableParams, $state) {

    $scope.table = new NgTableParams({
      count: 10,
      page: 1
    }, { getData: getData });

    function getData(params) {
      var name = params.filter().name ? params.filter().name : '';
      return $http.get('/api/suppliers?page=' + params.page() + '&limit=' + params.count() + '&name=' + name).then(function (response) {
        params.total(response.headers('x-pagination-total-count'));
        return response.data;
      }).catch(function (err) {
        if (err) console.log(err);
      });
    }

    $scope.onDelete = function (id, supplier) {
      if (confirm('Apakah Kamu Yakin Ingin Menghapus Data Ini ? ' + supplier.name)) {
        $http.delete('/api/suppliers/' + id).then(function (response) {
          alert('Berhasil Menghapus Data!');
          $state.reload();
        }).catch(function (err) {
          console.log(err);
          alert('Oopss!! Ada Kesalahan Saat Menghapus Data Dari Server!');
        });
      }
    };

  });