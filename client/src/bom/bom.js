angular.module('spk_deo')
  .config(function ($stateProvider) {
    $stateProvider
      .state('bom', {
        abstract: true,
        templateUrl: 'src/templates/_layout.html'
      })
      .state('bom.index', {
        url: '/boms',
        templateUrl: 'src/bom/bom.html',
        controller: 'BOMCtrl'
      })
      .state('bom.new', {
        url: '/boms/new',
        templateUrl: 'src/bom/form/bom.html',
        controller: 'BOMFormCtrl'
      })
      .state('bom.edit', {
        url: '/boms/edit/:id',
        templateUrl: 'src/bom/form/bom.html',
        controller: 'BOMFormCtrl'
      });
  });