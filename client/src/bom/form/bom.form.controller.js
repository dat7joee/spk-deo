angular.module('spk_deo')
  .controller('BOMFormCtrl', function ($scope, $http, $state, $stateParams) {
    $scope.model = {};
    $scope.model.materials = [];

    if ($stateParams.id) {
      $scope.onEdit = true;
      $http.get('/api/boms/' + $stateParams.id).then(function (response) {
        $scope.model = response.data;
      }).catch(function (err) {
        $state.go('^.index');
        console.log('err', err);
        alert('Oopss!! Data Tidak Ditemukan!');
      });
    }

    $scope.onSave = function () {
      if (confirm('Apakah Kamu Yakin Ingin Menyimpan Data Ini ?')) {
        if (!$stateParams.id) {
          $http.post('/api/boms', $scope.model).then(function (response) {
            alert('Berhasil Menyimpan!');
            $state.go('^.index');
          }).catch(function (err) {
            console.log(err);
            alert('Oopss! Error Terjadi Ketika Menghubungi Server!');
          });
        } else {
          $http.put('/api/boms/' + $stateParams.id, $scope.model).then(function (response) {
            alert('Berhasil Menyimpan!');
            $state.go('^.index');
          }).catch(function (err) {
            console.log(err);
            alert('Oopss! Error Terjadi Ketika Menghubungi Server!');
          });
        }
      }
    };

    $scope.onAddMaterial = function () {
      if (!$scope.holder.material && !$scope.holder.qty) return;

      $scope.model.materials.push({
        material: $scope.holder.material,
        qty: $scope.holder.qty
      });
      $scope.holder = {};
    };

    $scope.onRemoveMaterial = function (index) {
      $scope.model.materials.splice(index, 1);
    };

    $scope.getProducts = function (value) {
      return $http.get('/api/products/search?name=' + value).then(function (response) {
        return response.data;
      }).catch(function (err) {
        console.log(err);
      });
    };

    $scope.getMaterials = function (value) {
      return $http.get('/api/materials/search?name=' + value).then(function (response) {
        return response.data;
      }).catch(function (err) {
        console.log(err);
      });
    };
  });