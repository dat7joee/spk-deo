angular.module('spk_deo')
  .controller('EOQCtrl', function ($scope, $http, NgTableParams, $state, $stateParams) {
    $scope.eoq = {};

    $scope.table = new NgTableParams({
      count: 10,
      page: 1
    }, { getData: getData });

    function getData(params) {
      var month = params.filter().month ? params.filter().month : '';
      var year = params.filter().year ? params.filter().year : '';
      return $http.get('/api/eoqs?page=' + params.page() + '&limit=' + params.count() + '&month=' + month + '&year=' + year).then(function (response) {
        params.total(response.headers('x-pagination-total-count'));
        return response.data;
      }).catch(function (err) {
        if (err) console.log(err);
      });
    }

    $scope.onDelete = function (id, eoq) {
      if (confirm('Apakah Kamu Yakin Ingin Menghapus Data Ini ? ' + eoq.name)) {
        $http.delete('/api/eoqs/' + id).then(function (response) {
          alert('Berhasil Menghapus Data!');
          $state.reload();
        }).catch(function (err) {
          console.log(err);
          alert('Oopss!! Ada Kesalahan Saat Menghapus Data Dari Server!');
        });
      }
    };

    $scope.getEOQ = function (id) {
      $http.get('/api/eoqs/' + id).then(function (response) {
        $scope.eoq = response.data;
        console.log('eoq', $scope.eoq);
      }).catch(function (err) {
        $state.go('^.index');
        alert('Data Tidak Ditemukan!');
      });
    };

    if ($stateParams.id) {
      $scope.getEOQ($stateParams.id);
    }

    $scope.checkMonth = function (month) {
      month = Number(month);
      switch (month) {
        case 1:
          return 'Februari';
          break;
        case 2:
          return 'Maret';
          break;
        case 3:
          return 'April';
          break;
        case 4:
          return 'Mei';
          break;
        case 5:
          return 'Juni';
          break;
        case 6:
          return 'Juli';
          break;
        case 7:
          return 'Agustus';
          break;
        case 8:
          return 'September';
          break;
        case 9:
          return 'Oktober';
          break;
        case 10:
          return 'November';
          break;
        case 11:
          return 'Desember';
          break;
        default:
          return 'Januari';
      }
    }

  });