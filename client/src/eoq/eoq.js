angular.module('spk_deo')
  .config(function ($stateProvider) {
    $stateProvider
      .state('eoq', {
        abstract: true,
        templateUrl: 'src/templates/_layout.html'
      })
      .state('eoq.index', {
        url: '/eoqs',
        templateUrl: 'src/eoq/eoq.html',
        controller: 'EOQCtrl'
      })
      .state('eoq.edit', {
        url: '/eoqs/edit/:id',
        templateUrl: 'src/eoq/show.html',
        controller: 'EOQCtrl'
      });
  });