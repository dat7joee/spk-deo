angular.module('spk_deo')
  .controller('MaterialFormCtrl', function ($scope, $http, $state, $stateParams) {
    $scope.model = {};
    $scope.onEdit = false;
    if (!$stateParams.id) $scope.model.qty = 0;
    if ($stateParams.id) {
      $scope.onEdit = true;
      $http.get('/api/materials/' + $stateParams.id).then(function (response) {
        $scope.model = response.data;
      }).catch(function (err) {
        $state.go('^.index');
        console.log('err', err);
        alert('Oopss!! Data Tidak Ditemukan!');
      });
    }

    $scope.onSave = function () {
      if (confirm('Apakah Kamu Yakin Ingin Menyimpan Data Ini ?')) {
        if (!$stateParams.id) {
          $http.post('/api/materials', $scope.model).then(function (response) {
            alert('Berhasil Menyimpan!');
            $state.go('^.index');
          }).catch(function (err) {
            console.log(err);
            alert('Oopss! Error Terjadi Ketika Menghubungi Server!');
          });
        } else {
          $http.put('/api/materials/' + $stateParams.id, $scope.model).then(function (response) {
            alert('Berhasil Menyimpan!');
            $state.go('^.index');
          }).catch(function (err) {
            console.log(err);
            alert('Oopss! Error Terjadi Ketika Menghubungi Server!');
          });
        }
      }
    };


    $scope.getSuppliers = function (value) {
      return $http.get('/api/suppliers/search?name=' + value).then(function (response) {
        return response.data;
      }).catch(function (err) {
        console.log(err);
      });
    };
  });