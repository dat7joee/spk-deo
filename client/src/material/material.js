angular.module('spk_deo')
  .config(function ($stateProvider) {
    $stateProvider
      .state('material', {
        abstract: true,
        templateUrl: 'src/templates/_layout.html'
      })
      .state('material.index', {
        url: '/materials',
        templateUrl: 'src/material/material.html',
        controller: 'MaterialCtrl'
      })
      .state('material.new', {
        url: '/materials/new',
        templateUrl: 'src/material/form/material.html',
        controller: 'MaterialFormCtrl'
      })
      .state('material.edit', {
        url: '/materials/edit/:id',
        templateUrl: 'src/material/form/material.html',
        controller: 'MaterialFormCtrl'
      });
  });