angular.module('spk_deo')
  .controller('MaterialCtrl', function ($scope, $http, NgTableParams, $state) {

    $scope.table = new NgTableParams({
      count: 10,
      page: 1
    }, { getData: getData });

    function getData(params) {
      var name = params.filter().name ? params.filter().name : '';
      return $http.get('/api/materials?page=' + params.page() + '&limit=' + params.count() + '&name=' + name).then(function (response) {
        params.total(response.headers('x-pagination-total-count'));
        return response.data;
      }).catch(function (err) {
        if (err) console.log(err);
      });
    }

    $scope.onDelete = function (id, material) {
      if (confirm('Apakah Kamu Yakin Ingin Menghapus Data Ini ? ' + material.name)) {
        $http.delete('/api/materials/' + id).then(function (response) {
          alert('Berhasil Menghapus Data!');
          $state.reload();
        }).catch(function (err) {
          console.log(err);
          alert('Oopss!! Ada Kesalahan Saat Menghapus Data Dari Server!');
        });
      }
    };

    $scope.onResetStock = function () {
      if (confirm('Apakah Kamu Yakin Ingin Mereset Stock Material ? Aksi ini tidak bisa dikembalikan!!!')) {
        $http.get('/api/materials/reset').then(function (response) {
          alert('Berhasil Mereset Stock!');
          $scope.table.reload();
        }).catch(function (err) {
          console.log('err', err);
        });
      }
    };

    $scope.onAddStock = function (id) {
      var stock = prompt("Tambahkan Stock dalam Bal", 0);
      if (stock != null) {
        if (confirm('Apakah Kamu yakin ingin menambahkan Stock pada Produk Ini sebanyak ' + stock + ' Bal ?')) {
          $http.put('/api/materials/' + id + '/add-stock', {
            stock: Number(stock)
          }).then(function (response) {
            alert('Berhasil Menambahkan Stock Material!');
            $scope.table.reload();
          }).catch(function (err) {
            console.log('err', err);
          });
        }
      }
    }

  });