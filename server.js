var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var Config = require('./config');
var PORT = process.env.PORT || 5000;

var app = express();

mongoose.connect(Config.db);

mongoose.connection.on('connected', function () {
  console.log('Database Connected!');
});

mongoose.connection.on('error', function (err) {
  console.log('Failed to Connect Database', err);
});

app.use(express.static(path.join(__dirname, 'client')));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

require('./routes')(app);

app.listen(PORT, function () {
  console.log('Server Running on PORT', PORT);
});