var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var MaterialUsageSchema = new Schema({
  product: { type: Schema.Types.ObjectId, ref: 'Product' },
  productName: { type: String },
  qty: { type: Number, default: 0 },
  created: { type: Date, default: Date.now() }
});

module.exports = mongoose.model('MaterialUsage', MaterialUsageSchema);