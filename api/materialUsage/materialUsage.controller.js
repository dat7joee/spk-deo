var _ = require('lodash');
var Q = require('q');
var MaterialUsage = require('./materialUsage.model');
var Material = require('../material/material.model');
var BOM = require('../bom/bom.model');

exports.index = function (req, res) {
  var page = Number(req.query.page) || 1,
    limit = Number(req.query.limit) || 10,
    skip = (page - 1) * limit;
  var query = {
    productName: { $regex: req.query.productName, $options: 'i' }
  };
  Q.all([
    MaterialUsage.count(query).exec(),
    MaterialUsage.find(query).populate('product').skip(skip).limit(limit).exec()
  ])
    .spread(function (total, materialUsages) {
      res.set('X-Pagination-Total-Count', total);
      res.json(200, materialUsages);
    }).then(null, function (err) {
      return res.send(500, err);
    });
};

exports.show = function (req, res) {
  MaterialUsage.findOne({ _id: req.params.id }).populate('product').lean().exec(function (err, materialUsage) {
    if (err) return res.send(500, err);
    if (!materialUsage) return res.send(404);
    BOM.findOne({ product: materialUsage.product._id }).populate('materials.material').exec(function (err, bom) {
      if (err) return res.send(500, err);
      var mats = [];
      bom.materials.map(function (material) {
        var temp = materialUsage.qty * material.qty;
        mats.push({
          material: material.material.name,
          qty: temp
        });
      });

      materialUsage.materials = mats;
      res.json(200, materialUsage);
    });
  });
};


exports.showForRequest = function (req, res) {
  MaterialUsage.findOne({ _id: req.params.id }).populate('product').exec(function (err, materialUsage) {
    if (err) return res.send(500, err);
    if (!materialUsage) return res.send(404);

    return res.json(200, materialUsage)
  });
};

exports.create = function (req, res) {
  var body = req.body;
  body.productName = body.product.name;
  var promises = [];
  BOM.findOne({ product: body.product._id }).exec(function (err, bom) {
    if (err) return res.send(500, err);
    if (!bom) return res.send(404);

    bom.materials.map(function (material) {
      var value = body.qty * material.qty;
      promises.push(reduceStock(material.material, value));
    });
    Q.all(promises).spread(function (resultMaterial) {
      MaterialUsage.create(body, function (err, materialUsage) {
        if (err) return res.send(500, err);

        return res.json(201, materialUsage);
      });
    }).then(null, function (err) {
      return res.send(500, err);
    });
  });
};

function reduceStock(material, value) {
  return Material.findOne({ _id: material }).exec().then(function (material) {
    material.qty -= value;

    return material.save().then(function () {
      return material;
    }).then(null, function (err) {
      if (err) return null;
    });
  }).then(null, function (err) {
    if (err) return null;
  });
}

exports.update = function (req, res) {
  if (req.body._id) delete req.body._id;
  MaterialUsage.findOne({ _id: req.params.id }).exec(function (err, materialUsage) {
    if (err) return res.send(500, err);
    if (!materialUsage) return res.send(404);

    var updated = _.merge(materialUsage, req.body);
    updated.save(function (err) {
      if (err) return res.send(500, err);
      return res.json(200, updated);
    });
  });
};

function parseObjectToRefId(arr) {
  for (var i = 0; i < arr.length; i++) {
    if (arr[i].material._id) {
      var temp = _.clone(arr[i].material._id);
      arr[i].material = temp;
    }
    if (arr[i]._id) delete arr[i]._id;
  }
  return arr;
}

exports.delete = function (req, res) {
  MaterialUsage.findOne({ _id: req.params.id }).exec(function (err, materialUsage) {
    if (err) return res.send(500, err);
    if (!materialUsage) return res.send(404);

    materialUsage.remove(function (err) {
      if (err) return res.send(500, err);
      return res.json(204);
    });
  });
};