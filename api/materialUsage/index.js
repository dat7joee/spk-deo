var express = require('express');
var router = express.Router();
var controller = require('./materialUsage.controller');

router.get('/', controller.index);
router.get('/request/:id', controller.showForRequest);
router.get('/:id', controller.show);
router.post('/', controller.create);
router.put('/:id', controller.update);
router.delete('/:id', controller.delete);

module.exports = router;