var _ = require('lodash');
var Q = require('q');
var Supplier = require('./supplier.model');

exports.index = function (req, res) {
  var page = Number(req.query.page) || 1,
    limit = Number(req.query.limit) || 10,
    skip = (page - 1) * limit;
  var query = {
    name: { $regex: req.query.name, $options: 'i' }
  };
  Q.all([
    Supplier.count(query).exec(),
    Supplier.find(query).skip(skip).limit(limit).exec()
  ])
    .spread(function (total, suppliers) {
      res.set('X-Pagination-Total-Count', total);
      res.json(200, suppliers);
    }).then(null, function (err) {
      return res.send(500, err);
    });
};

exports.search = function (req, res) {
  Supplier.find({ name: { $regex: req.query.name, $options: 'i' } }, function (err, suppliers) {
    if (err) return res.send(500, err);
    return res.json(200, suppliers);
  });
};

exports.show = function (req, res) {
  Supplier.findOne({ _id: req.params.id }).exec(function (err, supplier) {
    if (err) return res.send(500, err);
    if (!supplier) return res.send(404);

    return res.json(200, supplier)
  });
};

exports.create = function (req, res) {
  var body = req.body;

  Supplier.create(body, function (err, supplier) {
    if (err) return res.send(500, err);

    return res.json(201, supplier);
  });
};

exports.update = function (req, res) {
  if (req.body._id) delete req.body._id;

  Supplier.findOne({ _id: req.params.id }).exec(function (err, supplier) {
    if (err) return res.send(500, err);
    if (!supplier) return res.send(404);

    var updated = _.merge(supplier, req.body);
    updated.save(function (err) {
      if (err) return res.send(500, err);
      return res.json(200, updated);
    });
  });
};

exports.delete = function (req, res) {
  Supplier.findOne({ _id: req.params.id }).exec(function (err, supplier) {
    if (err) return res.send(500, err);
    if (!supplier) return res.send(404);

    supplier.remove(function (err) {
      if (err) return res.send(500, err);
      return res.json(204);
    });
  });
};
