var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var SupplierSchema = new Schema({
  name: { type: String, trim: true },
  address: { type: String, trim: true },
  phone: { type: String, trim: true },
  created: { type: Date, default: Date.now() }
});

module.exports = mongoose.model('Supplier', SupplierSchema);