var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ForecastSchema = new Schema({
  product: { type: Schema.Types.ObjectId, ref: 'Product' },
  month: { type: String },
  year: { type: String },
  forecast: { type: Number, default: 0 },
  mse: { type: Number, default: 0 },
  processed: { type: Boolean, default: false },
  created: { type: Date, default: Date.now() }
});

module.exports = mongoose.model('Forecast', ForecastSchema);