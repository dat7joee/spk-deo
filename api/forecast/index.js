var express = require('express');
var router = express.Router();
var controller = require('./forecast.controller');

router.get('/', controller.index);
router.get('/reset', controller.deleteAll);
router.get('/eoq', controller.processEOQ);
router.get('/:id', controller.show);
router.post('/', controller.create);
router.put('/:id', controller.update);
router.delete('/:id', controller.delete);

module.exports = router;