var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var EOQSchema = new Schema({
  month: { type: String },
  year: { type: String },
  details: [
    {
      material: { type: Schema.Types.ObjectId, ref: 'Material' },
      name: { type: String },
      stock: { type: Number, default: 0 },
      realKebutuhan: { type: Number, default: 0 },
      kebutuhan: { type: Number, default: 0 },
      total: { type: Number, default: 0 },
      sisa: { type: Number, default: 0 },
      hasilBagi: { type: Number, default: 0 },
      bal: { type: Number, default: 0 },
      balEOQ: { type: Number, default: 0 },
      safetyStock: { type: Number, default: 0 }
    }
  ],
  created: { type: Date, default: Date.now() }
});

module.exports = mongoose.model('EOQ', EOQSchema);