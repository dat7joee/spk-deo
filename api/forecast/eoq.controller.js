var _ = require('lodash');
var Q = require('q');
var EOQ = require('./eoq.model');

exports.index = function (req, res) {
  var page = Number(req.query.page) || 1,
    limit = Number(req.query.limit) || 10,
    skip = (page - 1) * limit;
  var query = {
    month: { $regex: req.query.month, $options: 'i' },
    year: { $regex: req.query.year, $options: 'i' }
  };
  Q.all([
    EOQ.count(query).exec(),
    EOQ.find(query).skip(skip).limit(limit).exec()
  ])
    .spread(function (total, eoqs) {
      res.set('X-Pagination-Total-Count', total);
      res.json(200, eoqs);
    }).then(null, function (err) {
      return res.send(500, err);
    });
};

exports.show = function (req, res) {
  EOQ.findOne({ _id: req.params.id }).populate('details.material').exec(function (err, eoq) {
    console.log('eoq', eoq);
    if (err) return res.send(500, err);
    if (!eoq) return res.send(404);

    return res.json(200, eoq)
  });
};

exports.delete = function (req, res) {
  EOQ.findOne({ _id: req.params.id }).exec(function (err, eoq) {
    if (err) return res.send(500, err);
    if (!eoq) return res.send(404);

    eoq.remove(function (err) {
      if (err) return res.send(500, err);
      return res.json(204);
    });
  });
};