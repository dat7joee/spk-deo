var _ = require('lodash');
var Q = require('q');
var Forecast = require('./forecast.model');
var SalesOrder = require('../salesOrder/salesOrder.model');
var BOM = require('../bom/bom.model');
var EOQ = require('./eoq.model');

exports.index = function (req, res) {
  var page = Number(req.query.page) || 1,
    limit = Number(req.query.limit) || 10,
    skip = (page - 1) * limit;
  var query = {
    processed: false
  };
  Q.all([
    Forecast.count(query).exec(),
    Forecast.find(query).populate('product').skip(skip).limit(limit).exec()
  ])
    .spread(function (total, forecasts) {
      res.set('X-Pagination-Total-Count', total);
      res.json(200, forecasts);
    }).then(null, function (err) {
      return res.send(500, err);
    });
};

exports.show = function (req, res) {
  Forecast.findOne({ _id: req.params.id }).populate('product materials.material').exec(function (err, forecast) {
    if (err) return res.send(500, err);
    if (!forecast) return res.send(404);

    return res.json(200, forecast)
  });
};

exports.processEOQ = function (req, res) {
  var promises = [];
  var bomPromise = [];
  var bigForecasts = [];
  Forecast.find({ processed: false }).populate('products.product').exec(function (err, forecasts) {
    if (err) return res.send(500, err);
    if (forecasts.length <= 0) {
      return res.send(500, { message: "Belum ada Data Peramalan!" });
    } else {
      bigForecasts = forecasts;
      forecasts.map(function (forecast) {
        bomPromise.push(getProductBom(forecast.product, forecast.forecast));
      });
      Q.all(bomPromise).then(function (data) {
        var results = data;
        Q.all([extractResult(results)])
          .spread(function (bigMaterials) {
            Q.all([prosesKebutuhan(bigMaterials)]).spread(function (kebutuhanBB) {
              console.log('kebutuhanBB', kebutuhanBB);
              var plates = [];
              kebutuhanBB.map(function (material) {
                var total = material.total;
                var sisa = total % 250;
                var hasilBagi = Math.floor(total / 250);
                var konversi = sisa > 0 ? hasilBagi + 1 : hasilBagi;
                console.log(konversi, material.biayaPesan, material.biayaSimpan);
                var balEOQ = Math.ceil(Math.sqrt((2 * konversi * material.biayaPesan / material.biayaSimpan)));

                plates.push({
                  material: material.material,
                  name: material.name,
                  kebutuhan: material.kebutuhan,
                  safetyStock: material.safetyStock,
                  stock: material.stock,
                  total: total,
                  sisa: sisa,
                  hasilBagi: hasilBagi,
                  bal: konversi,
                  balEOQ: _.isNaN(balEOQ) ? 0 : balEOQ,
                  biayaPesan: material.biayaPesan,
                  biayaSimpan: material.biayaSimpan,
                  realKebutuhan: material.realKebutuhan
                });
              });
              console.log('plates', plates);
              var newEOQ = {
                month: req.query.month,
                year: req.query.year,
                details: plates
              };
              EOQ.create(newEOQ, function (err, eoq) {
                if (err) return res.send(500, err);

                res.json(200, { plates: plates, month: newEOQ.month, year: newEOQ.year });
              });
            });
          });
        // var bigMaterials = [];
        // results.map(function (result, index) {
        //   console.log('result index', index);
        //   result.materials.map(function (material) {
        //     var descIndex = bigMaterials.findIndex(function (mat) {
        //       return mat.material.toString() === material.material.toString();
        //     });

        //     if (descIndex > -1) {
        //       bigMaterials[descIndex].total += material.total;
        //     } else {
        //       bigMaterials.push({
        //         material: material.material,
        //         name: material.name,
        //         kebutuhan: material.total,
        //         stock: material.stock,
        //         safetyStock: material.stock * 0.1,
        //         biayaPesan: material.biayaPesan,
        //         biayaSimpan: material.biayaSimpan,
        //         realKebutuhan: material.total
        //       });
        //     }
        //     // if (bigMaterials.length > 0) {

        //     // } else {
        //     //   bigMaterials.push({
        //     //     material: material.material,
        //     //     name: material.name,
        //     //     kebutuhan: material.total,
        //     //     stock: material.stock,
        //     //     safetyStock: material.stock * 0.1,
        //     //     biayaPesan: material.biayaPesan,
        //     //     biayaSimpan: material.biayaSimpan,
        //     //     realKebutuhan: material.total
        //     //   });
        //     // }
        //   });
        // });
      }).then(null, function (err) {
        if (err) {
          console.log('err', err);
          return res.send(500, err);
        }
      });
    }
  });
};

function extractResult(results) {
  return new Promise(function (resolve, reject) {
    var bigMaterials = [];
    results.map(function (result, index) {
      result.materials.map(function (material) {
        var descIndex = bigMaterials.findIndex(function (mat) {
          return mat.material.toString() === material.material.toString();
        });

        if (descIndex > -1) {
          // bigMaterials[descIndex].kebutuhan += material.kebutuhan;
          bigMaterials[descIndex].total += material.total;
          bigMaterials[descIndex].realKebutuhan += material.realKebutuhan;
        } else {
          bigMaterials.push({
            material: material.material,
            name: material.name,
            kebutuhan: material.total,
            total: material.total,
            stock: material.stock,
            safetyStock: material.stock * 0.1,
            biayaPesan: material.biayaPesan,
            biayaSimpan: material.biayaSimpan,
            realKebutuhan: material.total
          });
        }
        // if (bigMaterials.length > 0) {

        // } else {
        //   bigMaterials.push({
        //     material: material.material,
        //     name: material.name,
        //     kebutuhan: material.total,
        //     stock: material.stock,
        //     safetyStock: material.stock * 0.1,
        //     biayaPesan: material.biayaPesan,
        //     biayaSimpan: material.biayaSimpan,
        //     realKebutuhan: material.total
        //   });
        // }
      });
    });
    console.log('end bigMaterials', bigMaterials);
    resolve(bigMaterials);
  });
}

function processingAllForecast(forecasts) {
  return Forecast.update({ processed: false }, { $set: { processed: true } }, { multi: true }).exec().then(function (result) {
    return result;
  }).then(null, function (err) {
    console.log('errr in update', err);
    if (err) return null;
  });
}

function prosesKebutuhan(materials) {
  return Q.Promise(function (resolve, reject) {
    var plates = [];
    materials.map(function (material) {
      // var kebutuhan = material.kebutuhan - material.stock;
      var kebutuhan = material.realKebutuhan - material.stock;
      var safetyStock = Math.ceil(material.realKebutuhan * 0.1);
      var total = kebutuhan + safetyStock;

      plates.push({
        material: material.material,
        name: material.name,
        stock: material.stock,
        kebutuhan: kebutuhan,
        safetyStock: safetyStock,
        total: total,
        biayaPesan: material.biayaPesan,
        realKebutuhan: material.realKebutuhan,
        biayaSimpan: material.biayaSimpan
      });
    });

    resolve(plates);
  });
}

function konversiKeBAL(materials) {
  var plates = [];
  return Q.Promise(function (resolve, reject) {


    resolve(plates);
  });
}

function lastProcessEOQ(materials) {

}

function getProductBom(product, forecast) {
  return BOM.findOne({ product: product._id }).populate('materials.material').exec().then(function (bom) {
    var plate = {
      product: product
    };

    var temp = [];
    bom.materials.map(function (material) {
      temp.push({
        material: material.material._id,
        name: material.material.name,
        stock: material.material.qty,
        total: forecast * material.qty,
        realKebutuhan: forecast * material.qty,
        biayaSimpan: material.material.biayaSimpan,
        biayaPesan: material.material.biayaPesan
      });
    });
    plate.materials = temp;

    return plate;
  }).then(null, function (err) {
    if (err) return null;
  });
}

exports.create = function (req, res) {
  var body = req.body;
  Forecast.findOne({ product: body.product._id, processed: false }).exec(function (err, forecast) {
    if (err) return res.send(500, err);

    if (forecast) {
      forecast.forecast = body.nextForecast;
      forecast.mse = body.MSE;
      forecast.month = body.month;
      forecast.year = body.year;
      forecast.save(function (err) {
        if (err) return res.send(500, err);
        return res.json(200, forecast);
      })
    } else {
      var newForecast = {
        product: body.product._id,
        forecast: body.nextForecast,
        mse: body.MSE,
        year: body.year,
        month: body.month
      };
      Forecast.create(newForecast, function (err, forecast) {
        if (err) return res.send(500, err);

        return res.json(201, forecast);
      });
    }
  });
};

exports.update = function (req, res) {
  if (req.body._id) delete req.body._id;

  Forecast.findOne({ _id: req.params.id }).exec(function (err, forecast) {
    if (err) return res.send(500, err);
    if (!forecast) return res.send(404);

    var updated = _.merge(forecast, req.body);
    updated.materials = req.body.materials;
    updated.markModified('materials');
    updated.save(function (err) {
      if (err) return res.send(500, err);
      return res.json(200, updated);
    });
  });
};

exports.delete = function (req, res) {
  Forecast.findOne({ _id: req.params.id }).exec(function (err, forecast) {
    if (err) return res.send(500, err);
    if (!forecast) return res.send(404);

    forecast.remove(function (err) {
      if (err) return res.send(500, err);
      return res.json(204);
    });
  });
};

exports.deleteAll = function (req, res) {
  Forecast.remove().exec(function (err, result) {
    if (err) return res.send(500, err);

    return res.json(200, result);
  });
};