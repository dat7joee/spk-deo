var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ProductSchema = new Schema({
  name: { type: String, trim: true },
  created: { type: Date, default: Date.now() }
});

module.exports = mongoose.model('Product', ProductSchema);