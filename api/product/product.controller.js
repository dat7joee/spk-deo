var _ = require('lodash');
var Q = require('q');
var Product = require('./product.model');

exports.index = function (req, res) {
  var page = Number(req.query.page) || 1,
    limit = Number(req.query.limit) || 10,
    skip = (page - 1) * limit;
  var query = {
    name: { $regex: req.query.name, $options: 'i' }
  };
  Q.all([
    Product.count(query).exec(),
    Product.find(query).skip(skip).limit(limit).exec()
  ])
    .spread(function (total, products) {
      res.set('X-Pagination-Total-Count', total);
      res.json(200, products);
    }).then(null, function (err) {
      return res.send(500, err);
    });
};

exports.search = function (req, res) {
  Product.find({ name: { $regex: req.query.name, $options: 'i' } }, function (err, products) {
    if (err) return res.send(500, err);
    return res.json(200, products);
  });
};

exports.show = function (req, res) {
  Product.findOne({ _id: req.params.id }).exec(function (err, product) {
    if (err) return res.send(500, err);
    if (!product) return res.send(404);

    return res.json(200, product)
  });
};

exports.create = function (req, res) {
  var body = req.body;

  Product.create(body, function (err, product) {
    if (err) return res.send(500, err);

    return res.json(201, product);
  });
};

exports.update = function (req, res) {
  if (req.body._id) delete req.body._id;

  Product.findOne({ _id: req.params.id }).exec(function (err, product) {
    if (err) return res.send(500, err);
    if (!product) return res.send(404);

    var updated = _.merge(product, req.body);
    updated.save(function (err) {
      if (err) return res.send(500, err);
      return res.json(200, updated);
    });
  });
};

exports.delete = function (req, res) {
  Product.findOne({ _id: req.params.id }).exec(function (err, product) {
    if (err) return res.send(500, err);
    if (!product) return res.send(404);

    product.remove(function (err) {
      if (err) return res.send(500, err);
      return res.json(204);
    });
  });
};