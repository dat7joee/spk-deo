var express = require('express');
var router = express.Router();
var controller = require('../forecast/eoq.controller');

router.get('/', controller.index);
router.get('/:id', controller.show);
router.delete('/:id', controller.delete);

module.exports = router;