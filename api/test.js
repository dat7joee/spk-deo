var _ = require('lodash');
var Q = require('q');
var express = require('express');
var router = express.Router();
var SalesOrder = require('../api/salesOrder/salesOrder.model');
var SMA = require('../components/deo/simpleMovingAverage').index;
var SES = require('../components/deo/singleExponentialSmoothing').index;
var RL = require('../components/deo/regresiLinear').index;
var DES = require('../components/deo/doubleExponentialSmoothing').index;
var DMA = require('../components/deo/doubleMovingAverage').index;

router.post('/', function (req, res) {
  // var data = {
  //   product: 'GT BLA BLA',
  //   requests: [25, 35, 33, 30, 39, 41, 45, 48, 46, 52, 50, 55]
  //   // requests: [35, 29, 48, 60, 55, 44, 42, 35, 75, 56, 55, 50] test untuk Regresi Linear
  //   // requests: [55, 65, 70, 62, 59, 54, 60, 68, 72, 70, 67, 65] test untuk SES
  //   // requests: [49, 65, 60, 53, 66, 46, 40, 50, 57, 42, 46, 38] test untuk SMA
  // };
  // var data = req.body;
  var data = req.body;
  for (var i = 0; i < data.requests.length; i++) {
    data.requests[i] = Number(data.requests[i]);
  }

  if (_.isEmpty(data.pattern)) return res.send(500, { message: "Pola Kosong!" });

  switch (data.pattern) {
    case 'horizontal':
      var smaResult = SMA(data);
      var sesResult = SES(data);
      var finalResult = null;
      if (sesResult.MSE < smaResult.MSE) {
        finalResult = sesResult;
      } else {
        finalResult = smaResult;
      }
      finalResult.product = data.product;
      res.json(finalResult);
      break;
    case 'trend':
      var desResults = DES(data);
      var dmaResults = DMA(data);
      var finalResult = null;
      if (desResults.MSE < dmaResults.MSE) {
        finalResult = desResults;
      } else {
        finalResult = dmaResults;
      }
      finalResult.product = data.product;
      res.json(finalResult);
      break;
    case 'musiman':
      var results = RL(data);
      results.product = data.product;
      res.json(results);
      break;
    case 'siklis':
      var sesResult = SES(data);
      var rlResult = RL(data);
      var finalResult = null;
      if (sesResult.MSE < rlResult.MSE) {
        finalResult = sesResult;
      } else {
        finalResult = rlResult;
      }
      finalResult.product = data.product;
      res.json(finalResult);
    default:
      res.send(500);
  }
  // Q.all([getSalesOrdersByYear(body.year, body.product._id)]).spread(function (result) {
  //   switch (body.pattern) {
  //     case 'horizontal':
  //       var smaResult = SMA(result);
  //       var sesResult = SES(result);
  //       var finalResult = null;
  //       if (sesResult.MSE > smaResult.MSE) {
  //         finalResult = sesResult;
  //       } else {
  //         finalResult = smaResult;
  //       }
  //       finalResult.product = result.product;
  //       res.json(finalResult);
  //       break;
  //     case 'trend':
  //       var desResults = DES(result);
  //       var dmaResults = DMA(result);
  //       var finalResult = null;
  //       if (desResults.MSE > dmaResults.MSE) {
  //         finalResult = desResults;
  //       } else {
  //         finalResult = dmaResults;
  //       }
  //       finalResult.product = result.product;
  //       res.json(finalResult);
  //       break;
  //     case 'musiman':
  //       var results = RL(result);
  //       results.product = result.product;
  //       res.json(results);
  //       break;
  //     case 'siklis':
  //       var sesResult = SES(data);
  //       var rlResult = RL(data);
  //       var finalResult = null;
  //       if (sesResult.MSE > rlResult.MSE) {
  //         finalResult = sesResult;
  //       } else {
  //         finalResult = rlResult;
  //       }
  //       finalResult.product = result.product;
  //       res.json(finalResult);
  //     default:
  //       res.send(500);
  //   }
  // }).then(null, function (err) {
  //   return res.send(500, err);
  // });
});

router.get('/years', function (req, res) {
  SalesOrder.find().select('year').exec(function (err, years) {
    if (err) return res.send(500, err);
    var yearArr = _.map(years, 'year');
    var diffYear = _.uniq(yearArr);
    return res.json(200, diffYear);
  });
});

// Another Function
function getSalesOrdersByYear(year, myProduct) {
  return SalesOrder.find({ year: year, products: { $elemMatch: { product: myProduct } } }).populate('products.product').sort({ month: 1 }).exec().then(function (salesOrders) {
    if (salesOrders.length < 12) {
      return { oke: false, message: "Data Tidak mencukupi 12 Bulan, Hanya tersedia " + salesOrders.length + " Bulan." }
    } else {
      var productSales = new Array(12);
      var productObject = null;
      salesOrders.map(function (salesOrder) {
        salesOrder.products.map(function (product) {
          productObject = product.product;
          if (product.product._id.toString() === myProduct.toString()) {
            productSales[salesOrder.month] = product.sale;
            // productSales.push({
            //   month: Number(salesOrder.month),
            //   sale: product.sale
            // });
          }
        });
      });

      return { oke: true, product: productObject, requests: productSales };
    }
  }).then(null, function (err) {
    if (err) return err;
  });
}

module.exports = router;