var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var BOMSchema = new Schema({
  product: { type: Schema.Types.ObjectId, ref: 'Product' },
  materials: [
    {
      material: { type: Schema.Types.ObjectId, ref: 'Material' },
      qty: { type: Number, default: 0 }
    }
  ],
  created: { type: Date, default: Date.now() }
});

module.exports = mongoose.model('BOM', BOMSchema);