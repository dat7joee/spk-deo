var _ = require('lodash');
var Q = require('q');
var BOM = require('./bom.model');

exports.index = function (req, res) {
  var page = Number(req.query.page) || 1,
    limit = Number(req.query.limit) || 10,
    skip = (page - 1) * limit;
  var query = {};
  Q.all([
    BOM.count(query).exec(),
    BOM.find(query).populate('product materials.material').skip(skip).limit(limit).exec()
  ])
    .spread(function (total, boms) {
      res.set('X-Pagination-Total-Count', total);
      res.json(200, boms);
    }).then(null, function (err) {
      return res.send(500, err);
    });
};

exports.show = function (req, res) {
  BOM.findOne({ _id: req.params.id }).populate('product materials.material').exec(function (err, bom) {
    if (err) return res.send(500, err);
    if (!bom) return res.send(404);

    return res.json(200, bom)
  });
};

exports.create = function (req, res) {
  var body = req.body;

  BOM.create(body, function (err, bom) {
    if (err) return res.send(500, err);

    return res.json(201, bom);
  });
};

exports.update = function (req, res) {
  var body = req.body;
  if (body._id) delete body._id;

  BOM.findOne({ _id: req.params.id }).populate('materials.material').exec(function (err, bom) {
    if (err) return res.send(500, err);
    if (!bom) return res.send(404);

    // var updated = _.merge(bom, body);
    _.extend(bom, body);
    // updated.materials = body.materials;
    // updated.markModified('materials');
    bom.save(function (err) {
      console.log('err', err);
      if (err) return res.send(500, err);
      return res.json(200, bom);
    });
  });
};

exports.delete = function (req, res) {
  BOM.findOne({ _id: req.params.id }).exec(function (err, bom) {
    if (err) return res.send(500, err);
    if (!bom) return res.send(404);

    bom.remove(function (err) {
      if (err) return res.send(500, err);
      return res.json(204);
    });
  });
};