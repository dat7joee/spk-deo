var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var MaterialSchema = new Schema({
  name: { type: String, trim: true },
  qty: { type: Number, default: 0 },
  biayaPesan: { type: Number, default: 0 },
  biayaSimpan: { type: Number, default: 0 },
  supplier: { type: Schema.Types.ObjectId, ref: 'Supplier' },
  created: { type: Date, default: Date.now() }
});

module.exports = mongoose.model('Material', MaterialSchema);