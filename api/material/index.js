var express = require('express');
var router = express.Router();
var controller = require('./material.controller');

router.get('/', controller.index);
router.get('/search', controller.search);
router.get('/reset', controller.resetStock);
router.get('/:id', controller.show);
router.post('/', controller.create);
router.put('/:id', controller.update);
router.put('/:id/add-stock', controller.addStock);
router.delete('/:id', controller.delete);

module.exports = router;