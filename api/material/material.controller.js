var _ = require('lodash');
var Q = require('q');
var Material = require('./material.model');

exports.index = function (req, res) {
  var page = Number(req.query.page) || 1,
    limit = Number(req.query.limit) || 10,
    skip = (page - 1) * limit;
  var query = {
    name: { $regex: req.query.name, $options: 'i' }
  };
  Q.all([
    Material.count(query).exec(),
    Material.find(query).populate('supplier').skip(skip).limit(limit).exec()
  ])
    .spread(function (total, materials) {
      res.set('X-Pagination-Total-Count', total);
      res.json(200, materials);
    }).then(null, function (err) {
      return res.send(500, err);
    });
};

exports.search = function (req, res) {
  Material.find({ name: { $regex: req.query.name, $options: 'i' } }, function (err, materials) {
    if (err) return res.send(500, err);
    return res.json(200, materials);
  });
};

exports.show = function (req, res) {
  Material.findOne({ _id: req.params.id }).populate('supplier').exec(function (err, material) {
    if (err) return res.send(500, err);
    if (!material) return res.send(404);

    return res.json(200, material)
  });
};

exports.create = function (req, res) {
  var body = req.body;
  body.qty *= 250;

  Material.create(body, function (err, material) {
    if (err) return res.send(500, err);

    return res.json(201, material);
  });
};

exports.update = function (req, res) {
  if (req.body._id) delete req.body._id;

  Material.findOne({ _id: req.params.id }).exec(function (err, material) {
    if (err) return res.send(500, err);
    if (!material) return res.send(404);

    var updated = _.merge(material, req.body);
    updated.save(function (err) {
      if (err) return res.send(500, err);
      return res.json(200, updated);
    });
  });
};

exports.addStock = function (req, res) {
  var stock = req.body.stock || 0;
  stock *= 250;
  Material.findOne({ _id: req.params.id }).exec(function (err, material) {
    if (err) return res.send(500, err);
    if (!material) return res.send(404);

    material.qty += stock;
    material.save(function (err) {
      if (err) return res.send(500, err);
      return res.json(200, material);
    });
  });
};

exports.resetStock = function (req, res) {
  Material.update({}, { $set: { qty: 0 } }, { multi: true }).exec(function (err, result) {
    if (err) return res.send(500, err);

    return res.json(200, result);
  });
};

exports.delete = function (req, res) {
  Material.findOne({ _id: req.params.id }).exec(function (err, material) {
    if (err) return res.send(500, err);
    if (!material) return res.send(404);

    material.remove(function (err) {
      if (err) return res.send(500, err);
      return res.json(204);
    });
  });
};