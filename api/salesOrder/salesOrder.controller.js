var _ = require('lodash');
var Q = require('q');
var SalesOrder = require('./salesOrder.model');

exports.index = function (req, res) {
  var page = Number(req.query.page) || 1,
    limit = Number(req.query.limit) || 10,
    skip = (page - 1) * limit;
  var query = {
    month: { $regex: req.query.month, $options: 'i' },
    year: { $regex: req.query.year, $options: 'i' }
  };
  Q.all([
    SalesOrder.count(query).exec(),
    SalesOrder.find(query).populate('products.product').skip(skip).limit(limit).exec()
  ])
    .spread(function (total, salesOrders) {
      res.set('X-Pagination-Total-Count', total);
      res.json(200, salesOrders);
    }).then(null, function (err) {
      return res.send(500, err);
    });
};

exports.show = function (req, res) {
  SalesOrder.findOne({ _id: req.params.id }).populate('products.product').exec(function (err, salesOrder) {
    if (err) return res.send(500, err);
    if (!salesOrder) return res.send(404);

    return res.json(200, salesOrder)
  });
};

exports.create = function (req, res) {
  var now = new Date();
  var body = req.body;
  body.month = Number(body.month);
  body.periode = new Date(body.year, body.month, 1, 0, 0, 1);
  SalesOrder.count({ month: body.month, year: body.year, periode: body.periode }).exec(function (err, count) {
    if (err) return res.send(500, err);

    if (count > 0) {
      return res.send(500, { message: "Sales Order dengan Bulan dan Tahun Tersebut sudah ada!" });
    } else {
      SalesOrder.create(body, function (err, salesOrder) {
        if (err) return res.send(500, err);

        return res.json(201, salesOrder);
      });
    }
  });
};

exports.update = function (req, res) {
  if (req.body._id) delete req.body._id;

  SalesOrder.findOne({ _id: req.params.id }).exec(function (err, salesOrder) {
    if (err) return res.send(500, err);
    if (!salesOrder) return res.send(404);

    // var updated = _.merge(salesOrder, req.body);
    _.extend(salesOrder, req.body);
    salesOrder.save(function (err) {
      if (err) return res.send(500, err);
      return res.json(200, salesOrder);
    });
  });
};

exports.delete = function (req, res) {
  SalesOrder.findOne({ _id: req.params.id }).exec(function (err, salesOrder) {
    if (err) return res.send(500, err);
    if (!salesOrder) return res.send(404);

    salesOrder.remove(function (err) {
      if (err) return res.send(500, err);
      return res.json(204);
    });
  });
};