var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var SalesOrderSchema = new Schema({
  month: { type: String, enum: ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11'], trim: true },
  year: { type: String, trim: true },
  periode: { type: Date },
  products: [
    {
      product: { type: Schema.Types.ObjectId, ref: 'Product' },
      sale: { type: Number, default: 0 }
    }
  ],
  created: { type: Date, default: Date.now() }
});

module.exports = mongoose.model('SalesOrder', SalesOrderSchema);