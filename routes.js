module.exports = function (app) {
  app.use('/api/materials', require('./api/material'));
  app.use('/api/products', require('./api/product'));
  app.use('/api/materialUsages', require('./api/materialUsage'));
  app.use('/api/salesOrders', require('./api/salesOrder'));
  app.use('/api/boms', require('./api/bom'));
  app.use('/api/test', require('./api/test'));
  app.use('/api/forecast', require('./api/forecast'));
  app.use('/api/eoqs', require('./api/eoq'));
  app.use('/api/suppliers', require('./api/supplier'));
};